import logo from "../../images/pstulogo.webp";
import './home.sass';

import React, {useEffect} from 'react';

import Navbar from "../../components/navbar";
import MainButton from "../../components/main-button";
import OutlineButton from "../../components/outline-button";
import Advantage from "../../components/advantage";
import CardList from "../../components/card-list";
import Footer from "../../components/footer";
import {useDispatch, useSelector} from "react-redux";
import {getGroups} from "../../redux/actions/admin-group";

const Home = () => {

  const dispatch = useDispatch();
  const groups = useSelector(state => state.adminGroup.groups);

  useEffect(() => {
    document.title = 'Портал магістрів ПДТУ';
    dispatch(getGroups());
  }, []);

  const user = useSelector(state => state.userExternal.user);

  return (
    <React.Fragment>
      <Navbar/>
      <div className="container home-page-first-screen">
        <div className="row">
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-12">
                <h1>Вітаємо на порталі магістрів <span>ПДТУ</span></h1>
              </div>
              <div className="col-lg-12">
                <p className="subTitle">На даному порталі можна знайти цікаві для Вас теми дипломних робіт, а також
                  опублікувати свою</p>
              </div>
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-12 home-page-buttons-wrapper">
                    {
                      user === false ? <MainButton onClick={() => alert('Вход в личный кабинет')}>Реєстрація</MainButton> : null
                    }
                    <OutlineButton onClick={() => alert('Регистрация')}>Особистий кабінет</OutlineButton>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <img src={logo} alt="Pstu"/>
          </div>
        </div>
        <div className="row home-advantages">
          <div className="col-lg-4">
            <Advantage
              icon="fas fa-layer-group"
              title="Под собой интеллигибельный"
              subtitle="Надстройка нетривиальна. Сомнение рефлектирует естественный закон исключённого третьего.."
            />
          </div>
          <div className="col-lg-4">
            <Advantage
              icon="fab fa-sketch"
              title="Под собой интеллигибельный"
              subtitle="Надстройка нетривиальна. Сомнение рефлектирует естественный закон исключённого третьего.."
            />
          </div>
          <div className="col-lg-4">
            <Advantage
              icon="fas fa-code"
              title="Под собой интеллигибельный"
              subtitle="Надстройка нетривиальна. Сомнение рефлектирует естественный закон исключённого третьего.."
            />
          </div>
        </div>
      </div>
      <hr className="page-delimiter"/>

      <section className="personal-sites-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h2>Персональні <span>сторінки</span> магістрів</h2>
            </div>
          </div>
          <div className="row">
            <CardList groups={groups} />
          </div>
        </div>
      </section>
      <Footer/>
    </React.Fragment>
  );
};

export default Home;