import './account-edit.sass';

import React from 'react';
import Navbar from "../../components/navbar";
import Footer from "../../components/footer";
import AccountEditForm from "../../components/account-edit-form";

const AccountEdit = () => {
  return (
    <React.Fragment>
      <Navbar />
      <main className="account-edit-main">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <AccountEditForm />
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default AccountEdit;