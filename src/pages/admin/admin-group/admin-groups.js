import React, {useEffect} from 'react';
import Navbar from "../../../components/navbar";
import {Link} from "react-router-dom";
import Footer from "../../../components/footer";
import {useDispatch, useSelector} from "react-redux";
import {getGroups} from "../../../redux/actions/admin-group";
import GroupItem from "../../../components/admin/group-item";
import GroupItemAdd from "../../../components/admin/group-item-add";

const AdminGroup = () => {

  const dispatch = useDispatch();

  const groups = useSelector(state => state.adminGroup.groups);

  useEffect(() => {
    dispatch(getGroups());
  }, []);

  const AdminGroupList = groups.map((item) => {
    return (
      <GroupItem id={item.id} name={item.name} key={item.id} />
    );
  });

  return(
    <React.Fragment>
      <Navbar />
      <div className="container">
        <div className="row" style={{margin: '50px 0'}}>
          <div className="col-12">
            <h1 style={{textAlign: 'center'}}>Групи</h1>
          </div>

          <GroupItemAdd />
          {AdminGroupList}

        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
}

export default AdminGroup;