import React from 'react';
import Navbar from "../../../components/navbar";
import Footer from "../../../components/footer";
import {Link} from "react-router-dom";

const Dashboard = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className="container">
        <div className="row" style={{margin: '50px 0'}}>
          <div className="col-lg-4">
            <Link to="/admin/groups">
              <p>Групи</p>
            </Link>
          </div>
          <div className="col-lg-4">
            <Link to="/admin/users">
              <p>Користувачі</p>
            </Link>
          </div>
        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
};

export default Dashboard;