import './account.sass';

import React, {useEffect} from 'react';
import Navbar from "../../components/navbar";
import Footer from "../../components/footer";
import AccountHeader from "../../components/account-header";
import AccountContent from "../../components/account-content";
import {useDispatch, useSelector} from "react-redux";
import {getUserById} from "../../redux/actions/public-account";
import Redirect from "react-router-dom/es/Redirect";

const Account = (props) => {

  const dispatch = useDispatch();

  const user = useSelector(state => state.publicAccount);

  useEffect(() => {
    const accountId = props.match.params.id;
    dispatch(getUserById({user_id: accountId}));
  }, [props.match.params.id]);


  if (user.isLoaded) {
    if (user.user) {
      return (
        <React.Fragment>
          <Navbar />
          <AccountHeader />
          <AccountContent />
          <Footer />
        </React.Fragment>
      );
    } else {
      return (
        <Redirect push to="/not-found" />
      );
    }
  } else {
    return false;
  }
};

export default Account;