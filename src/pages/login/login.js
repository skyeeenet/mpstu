import '../../styles/forms.sass'
import loginImage from '../../images/login.png';

import React from 'react';

import Navbar from "../../components/navbar";
import LoginForm from "../../components/login-form";
import {login} from "../../services/webService";
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import {authLogin} from "../../redux/actions/auth";

const Login = () => {

  const dispatch = useDispatch();

  const message = useSelector(state => state.auth.message);

  const onSubmitHandler = (email, password) => {

    const formData = {
      email,
      password,
      cb: () => {
        return (
          <Redirect to="/" />
        );
      },
    };

    dispatch(authLogin(formData));
  };

  return (
    <React.Fragment>
      <Navbar/>
      <div className="container">
        <div className="row">
          <div className="col-lg-6 action-form-wrapper">
            <LoginForm submitHandler={onSubmitHandler} />
            <p>{message}</p>
          </div>
          <div className="right-form-image" style={{backgroundImage: `url(${loginImage})`}}>
            <div className="right-form-image-styler"></div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Login;