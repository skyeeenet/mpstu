import './user-list.sass';

import React, {useEffect} from 'react';
import Navbar from "../../components/navbar";
import Footer from "../../components/footer";
import UserItem from "../../components/user-item";
import {useDispatch, useSelector} from "react-redux";
import {groupUsers} from "../../redux/actions/user";
import {Link} from "react-router-dom";

const UserList = (props) => {

  const users = useSelector(state => state.groupUsers.users);
  const group = useSelector(state => state.groupUsers.group);
  const dispatch = useDispatch();

  useEffect(() => {
    const groupId = props.match.params.id;
    dispatch(groupUsers(groupId));
  }, []);

  console.log(group);

  return(
    <React.Fragment>
      <Navbar />
      <main className="user-list-items">
        <h1 style={{textAlign: 'center'}}>{group.name}</h1>
        <div className="container">
          <div className="row">
            {
              users.map((item) => {
              return (
                <div key={item.id} className="col-md-3">
                  <Link to={`/account/${item.id}`}>
                    <UserItem key={item.id} name={item.name} image={item.profile_image} theme={item.theme} />
                  </Link>
                </div>
              )
              })
            }
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default UserList;