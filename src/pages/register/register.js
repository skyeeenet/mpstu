import '../../styles/forms.sass'
import register from '../../images/register.png';

import React from 'react';
import Navbar from "../../components/navbar";
import RegisterForm from "../../components/register-form/register-form";

const Register = () => {

  return (
    <React.Fragment>
      <Navbar/>
      <div className="container">
        <div className="row">
          <div className="col-lg-6 action-form-wrapper">
            <RegisterForm />
          </div>
          <div className="right-form-image" style={{backgroundImage: `url(${register})`}}>
            <div className="right-form-image-styler"></div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Register;