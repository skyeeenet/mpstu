const getRandomArrayItem = (items) => {
  return items[Math.floor(Math.random()*items.length)];
};

export {getRandomArrayItem};