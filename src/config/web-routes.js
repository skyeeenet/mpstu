const baseUrl = 'http://googleservice.loc';

const webRoutes = {
  login: `${baseUrl}/login`,
  register: `${baseUrl}/register`,
  logout: `${baseUrl}/logout`,
  group: `${baseUrl}/group`,
  user: `${baseUrl}/get_user`,
  userId: `${baseUrl}/get_user_id`,
  editAccount: `${baseUrl}/user/update`,
  editAccountPhoto: `${baseUrl}/user/photo/update`,
  account: `${baseUrl}/account`,

  groupUsers: `${baseUrl}/groups/users`,
  groups: `${baseUrl}/admin/groups`,
  addGroup: `${baseUrl}/admin/group/create`,
  updateGroup: `${baseUrl}/admin/group/update`,
  deleteGroup: `${baseUrl}/admin/group/delete`,
};

export {webRoutes};