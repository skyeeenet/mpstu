import React from 'react';
import {Switch, Redirect, Route} from 'react-router-dom';
import Home from "./pages/home/home";
import Login from "./pages/login";
import Register from "./pages/register";
import Account from "./pages/account";
import AccountEdit from "./pages/account-edit";
import UserList from "./pages/user-list";
import Dashboard from "./pages/admin/dashboard/dashboard";
import AdminGroup from "./pages/admin/admin-group/admin-groups";
import AdminAccountEditForm from "./components/admin/admin-account-edit-form";

const Routes = () => {
  return (
    <Switch>
      <Route component={Home}
             exact
             path="/"/>
      <Route component={Login}
             exact
             path="/login"/>
      <Route component={Register}
             exact
             path="/register"/>
      <Route component={UserList}
             exact
             path="/group/:id"/>
      <Route component={AccountEdit}
             exact
             path="/account/edit"/>
      <Route component={Account}
             exact
             path="/account/:id"/>

      <Route component={Dashboard}
             exact
             path="/admin"/>

      <Route component={AdminGroup}
             exact
             path="/admin/groups"/>


      <Route component={AdminAccountEditForm}
             exact
             path="/admin/account/edit/:id"/>

      <Redirect to="/not-found"/>
    </Switch>
  )
}

export default Routes;