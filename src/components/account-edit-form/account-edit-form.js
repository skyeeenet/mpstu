import './account-edit-form.sass';
import profileLogo from '../../images/selfie.webp';

import React, {useEffect} from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import {useDispatch, useSelector} from "react-redux";
import {changeField, updateUser, updateUserPhoto} from "../../redux/actions/user";
import {getGroups} from "../../redux/actions/admin-group";

const AccountEditForm = (props) => {

  const groups = useSelector(state => state.adminGroup.groups);
  const user = useSelector(state => state.userExternal);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getGroups());
  }, [user.id]);

  const modules = {
    toolbar: [
      [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
      [{size: []}],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'},
        {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image', 'video'],
      ['clean'],
      [{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }]
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    }
  };

  const formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'video'
  ];

  const onChangeContent = (content) => {
    //setAccountTextState(content);
    dispatch(changeField({field: 'content', value: content}));
  };

  const onChangeHandler = (e) => {
    const elName = e.target.name;
    dispatch(changeField({field: elName, value: e.target.value}));
  };

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(updateUser(user));
  };

  const onFileChange = (e) => {
    const file = e.target.files[0];
    const data = new FormData();
    data.append('photo', file, file.name);
    dispatch(updateUserPhoto({id: user.id, photo: data}));
  };

  return (
    <form className="account-edit-form" encType="multipart/form-data" action="#" onSubmit={onSubmit}>
      <p className="action-form-title">Налаштування профілю</p>
      <div className="form-group">
        <label>Изображение профиля</label>
        <div className="account-edit-image-wrapper">
          <div className="account-edit-image" style={{backgroundImage: `url(${user.image})`}}></div>
          <input type="file"
                 name="profile_picture"
                 onChange={onFileChange}
          />
        </div>
      </div>

      <div className="form-group">
        <label>E-mail</label>
        <input className="action-form-input"
               placeholder="E-mail *"
               value={user.email}
               type="email"
               onChange={onChangeHandler}
               name="email"/>
      </div>

      <div className="form-group">
        <label>Пароль</label>
        <input className="action-form-input"
               placeholder="Пароль"
               value={user.password}
               type="password"
               onChange={onChangeHandler}
               name="password"/>
      </div>

      <div className="form-group">
        <label>Ім'я</label>
        <input className="action-form-input"
               placeholder="Ім'я *"
               value={user.name}
               onChange={onChangeHandler}
               type="text"
               name="name"/>
      </div>

      <div className="form-group">
        <label>Факультет</label>
        <input className="action-form-input"
               placeholder="Факультет *"
               value={user.faculty}
               type="text"
               onChange={onChangeHandler}
               name="faculty"/>
      </div>

      <div className="form-group">
        <label>Група</label>
        <select name="group" className="action-form-input" onChange={onChangeHandler}>
          {
            groups.map((item) => {
              return(
                <option selected={item.id == user.group} value={item.id}>{item.name}</option>
              )
            })
          }
        </select>
      </div>

      <div className="form-group">
        <label>Тема диплома</label>
        <input className="action-form-input"
               placeholder="Тема диплома"
               value={user.theme}
               type="text"
               onChange={onChangeHandler}
               name="theme"/>
      </div>

      <div className="form-group">
        <label>Теза</label>
        <input className="action-form-input"
               placeholder="Теза"
               value={user.short_description}
               type="text"
               onChange={onChangeHandler}
               name="short_description"/>
      </div>

      <div className="form-group">
        <label>Текстовий опис профілю</label>
        <ReactQuill value={user.content}
                    onChange={onChangeContent}
                    modules={modules}
                    formats={formats} />
      </div>

      <div className="form-group">
        <button className="account-edit-form-button" type="submit">Оновити</button>
      </div>

    </form>
  );
};

export default AccountEditForm;