import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {addGroup} from "../../../redux/actions/admin-group";

const GroupItemAdd = () => {

  const [name, setName] = useState('');
  const dispatch = useDispatch();

  const setNameHandler = (e) => {
    setName(e.target.value);
  };

  const addGroupHandler = (e) => {
    e.preventDefault();
    dispatch(addGroup(name));
  }

  return(
    <div className="col-lg-3" style={{border: '1px solid gray', margin: '20px'}}>
      <form onSubmit={addGroupHandler} className="d-flex flex-column">
        <p>Додати групу</p>
        <input type="text"
               name="name"
               style={{marginBottom: '5px'}}
               value={name}
               onChange={setNameHandler}/>
        <button type="submit">Додати</button>
      </form>
    </div>
  )
};

export default GroupItemAdd;