import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {deleteGroup, updateGroup} from "../../../redux/actions/admin-group";

const GroupItem = ({id, name}) => {

  const [nameInner, setName] = useState(name);
  const dispatch = useDispatch();

  const onChangeHandler = (e) => {
    setName(e.target.value);
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();
    dispatch(updateGroup({id, name: nameInner}));
  };

  const onDeleteHandler = (e) => {
    e.preventDefault();
    dispatch(deleteGroup(id));
  };

  return(
    <div className="col-lg-3" style={{border: '1px solid gray', margin: '20px'}}>
      <form onSubmit={onSubmitHandler}>
        <input type="text"
               name="name"
               style={{marginBottom: '5px'}}
               value={nameInner}
               onChange={onChangeHandler}/>
        <button type="submit">Оновити</button>
        <button onClick={onDeleteHandler} type="button">Видалити</button>
      </form>
    </div>
  );
};

export default GroupItem;
