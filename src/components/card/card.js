import './card.sass';

import React from 'react';

const Card = ({styles, title, text, id}) => {
  console.log(title);
  return (
    <div className="card-item">
      <span style={{backgroundColor: styles.background}} className="card-icon">
        <i style={{color: styles.color}} className={styles.icon}></i>
      </span>
      <p className="card-title">{title}</p>
      <p className="card-text">{text}</p>
      <span className="card-item-link"><i className="fas fa-long-arrow-alt-right"></i></span>
    </div>
  );
};

export default Card;