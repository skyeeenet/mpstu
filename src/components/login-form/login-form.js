import '../../styles/forms.sass';

import React, {useState} from 'react';
import {Link} from "react-router-dom";
import MainButton from "../main-button";
import {validateLength} from "../../services/validate";

const LoginForm = ({submitHandler}) => {

  const [emailState, setEmailState] = useState('');
  const [passwordState, setPasswordState] = useState('');

  const emailHandler = (e) => {

    if (validateLength(e.target, 5))
      e.target.classList.remove('inputError');
    else
      e.target.classList.add('inputError');

    setEmailState(e.target.value);
  };

  const passwordHandler = (e) => {

    if (validateLength(e.target, 8))
      e.target.classList.remove('inputError');
    else
      e.target.classList.add('inputError');

    setPasswordState(e.target.value);
  }

  const onSubmit = (e) => {
    e.preventDefault();
    submitHandler(emailState, passwordState);
  }

  return (
    <form onSubmit={onSubmit} className="action-form" action="#">
      <p className="action-form-title">Увійти</p>
      <p className="action-form-subtitle">Немає облікового запису?
        <Link to="/register">Створити <i className="fas fa-long-arrow-alt-right"></i></Link>
      </p>
      <input value={emailState} onChange={emailHandler}
             className="action-form-input" placeholder="E-mail *" type="email" name="email"/>

      <input className="action-form-input"
             value={passwordState}
             onChange={passwordHandler}
             placeholder="Password *"
             type="password"
             name="password"/>
      <MainButton type="button">Увійти</MainButton>
    </form>
  );
};

export default LoginForm;