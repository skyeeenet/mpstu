import './account-content.sass';

import React from 'react';
import {useSelector} from "react-redux";

const AccountContent = () => {

  const user = useSelector(state => state.publicAccount);

  return (
    <main className="profile-content">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h1 className="profile-name">{user.name}</h1>
            <p className="profile-subtitle">{user.short_description}</p>
          </div>
        </div>
        <div className="row account-info">
          <div className="col-lg-4">
            <div className="account-column-info">
              <span>{user.faculty}</span>
              <b>Факультет</b>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="account-column-info">
              <span>{user.group.name}</span>
              <b>Група</b>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="account-column-info">
              <span>{user.theme}</span>
              <b>Тема диплома</b>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="account-text" dangerouslySetInnerHTML={{__html: user.content}} />
          </div>
        </div>
      </div>
    </main>
  );
};

export default AccountContent;