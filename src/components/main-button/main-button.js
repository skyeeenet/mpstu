import './main-button.css';

import React from 'react';

const MainButton = ({children, onClick, type = 'a', to = ''}) => {

  const CustomTag = type;

  return (
    <CustomTag onClick={onClick} to={to} className="waves-effect waves-light btn main-btn">{children}</CustomTag>
  );
}

export default MainButton;