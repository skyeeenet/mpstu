import React from 'react';
import Card from "../card";
import {getRandomArrayItem} from "../../helpers/helpers";
import {Link} from "react-router-dom";

const CardList = ({groups}) => {

  const styles = [
    {color: "rgb(33, 150, 243)", background: "#e3f2fd", icon: "fas fa-pen-nib"},
    {color: "rgb(156, 39, 176)", background: "#f3e5f5", icon: "fas fa-book-open"},
    {color: "rgb(255, 193, 7)", background: "#fff8e1", icon: "fas fa-camera-retro"},
  ];

  return groups.map((item) => {

    console.log(item);

    return (
      <div key={item.id} className="col-lg-3 col-md-4 col-sm-6 col-12">
        <Link to={`/group/${item.id}`}>
          <Card
            key={item.id}
            title={item.name}
            text={"Кількість профілів: " + item.users.length}
            styles={getRandomArrayItem(styles)}
          />
        </Link>
      </div>
    )
  });
}

export default CardList;