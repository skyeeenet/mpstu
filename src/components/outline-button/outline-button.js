import React from 'react';
import './outline.css';

const OutlineButton = ({children, onClick}) => {
  return(
    <a onClick={onClick} className="waves-effect waves-light btn outline-btn">{children}</a>
  );
}

export default OutlineButton;