import '../../styles/forms.sass'
import React, {useEffect} from 'react';
import {Link} from "react-router-dom";
import MainButton from "../main-button";
import {useDispatch, useSelector} from "react-redux";
import {getGroups} from "../../redux/actions/admin-group";

const RegisterForm = ({submitHandler}) => {

  const dispatch = useDispatch();
  const groups = useSelector(state => state.adminGroup.groups);

  useEffect(() => {
    dispatch(getGroups());
  }, []);

  return (
    <form onSubmit={submitHandler} className="action-form" action="#">
      <p className="action-form-title">Створити аккаунт</p>
      <p className="action-form-subtitle">Вже є аккаунт?
        <Link to="/login">Увійти <i className="fas fa-long-arrow-alt-right"></i></Link>
      </p>
      <input className="action-form-input" placeholder="Прізвище та ім'я" type="text" name="text"/>
      <select name="group" className="action-form-input">
        {
          groups.map((item) => {
            return (
              <option value={item.id}>{item.name}</option>
            )
          })
        }
      </select>
      <input className="action-form-input" placeholder="E-mail *" type="email" name="email"/>
      <MainButton type="button">Створити</MainButton>
    </form>
  );
};

export default RegisterForm;