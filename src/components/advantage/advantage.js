import './advantage.sass';

import React from 'react';

const Advantage = ({icon, title, subtitle}) => {

  return (
    <div className="advantage-item">
      <span className="advantage-icon">
        <i className={icon}></i>
      </span>
      <p className="advantage-item-title">{title}</p>
      <p className="advantage-item-subtitle">{subtitle}</p>
    </div>
  );
};

export default Advantage;