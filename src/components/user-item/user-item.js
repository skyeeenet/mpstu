import './user-item.sass';
import userPicture from '../../images/selfie.webp';

import React from 'react';

const UserItem = ({name, image, theme}) => {
  return (
    <div className="user-item-wrapper">
      <div className="user-item-image" style={{backgroundImage: `url(${image})`}}></div>
      <p className="user-item-title">{name}</p>
      <p className="user-item-subtitle"><b>Тема диплому:</b> <br/>{theme}</p>
    </div>
  );
};

export default UserItem;