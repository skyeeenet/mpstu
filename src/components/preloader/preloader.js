import './preloader.css';

import React from 'react';
import {useDispatch, useSelector} from "react-redux";

const Preloader = () => {

  const isShow = useSelector(state => state.helper.preloader);

  const preloader = () => {
    if (isShow) {
      return (
        <div className="preloader-wrapper">
          <div className="lds-dual-ring"></div>
        </div>
      )
    } else {
      return null;
    }
  }

  return preloader();
};

export default Preloader;
