import './account-header.sass';
import profilePicture from '../../images/selfie.webp';

import React from 'react';
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import OutlineButton from "../outline-button";
import Redirect from "react-router-dom/es/Redirect";

const AccountHeader = () => {

  const user = useSelector(state => state.publicAccount);
  const currentUser = useSelector(state => state.userExternal);

  return (
    <header className="account-header">
      <div className="container">
        <div className="row">
          <div className="col-12 account-image-wrapper">
            <div className="account-image" style={{backgroundImage: `url(${user.image})`}}></div>
            {
              currentUser.id == user.id ? <Link to={`/account/edit`}>Редагувати</Link> : null
            }
            {
              currentUser.role == 'admin' ? <Link to={`/admin/account/edit/${user.id}`}>Редагувати як адмiн</Link> : null
            }
          </div>
        </div>
      </div>
    </header>
  );

};

export default AccountHeader;