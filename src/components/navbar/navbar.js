import React from 'react';
import './navbar.css';
import MainButton from "../main-button";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

const Navbar = () => {

  const user = useSelector(state => state.userExternal);
  const userAccountLink = `/account/${user.id}`;

  const AuthLinks = () => {
    return (
      <React.Fragment>
        <li>
          <Link to={userAccountLink}>Особистий кабінет</Link>
        </li>
        <li><a href="#">Вийти</a></li>
      </React.Fragment>
    );
  }

  const GuestLinks = () => {
    return (
      <React.Fragment>
        <li>
          <MainButton type={Link} to="/login">Увійти</MainButton>
        </li>
        <li>
          <Link to="/register">Реєстрація</Link>
        </li>
      </React.Fragment>
    )
  }

  return (
    <nav className="navbar">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="nav-wrapper">
                            <span className="brand-logo">
                                <Link to="/">M PSTU</Link>
                            </span>
              <ul id="nav-mobile" className="right hide-on-med-and-down">
                {user.user !== false ? <AuthLinks/> : <GuestLinks/>}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;