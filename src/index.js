import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import 'materialize-css/dist/js/materialize.min';
//import 'materialize-css/dist/css/materialize.min.css';
import 'bootstrap-4-grid/css/grid.min.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import store from "./redux/store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
