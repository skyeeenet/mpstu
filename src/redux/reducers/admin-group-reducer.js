const initState = {
  groups: [],
};

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

const adminGroupReducer = (state = initState, action) => {

  switch(action.type) {
    case 'ADMIN_GROUPS_INIT':
      return {
        ...state,
        groups: action.payload,
      };

    case 'ADMIN_GROUPS_ADD':
      return {
        ...state,
        groups: [
          ...state.groups,
          {name: action.payload, id: getRandomArbitrary(10000,99999999999), users: []}
        ]
      };

    case 'ADMIN_GROUPS_UPDATE':
      return {
        ...state,
        groups: state.groups.map((item) => {
          if (item.name == action.payload.name) {
            return {
              id: item.id,
              name: action.payload.name,
              users: item.users,
            }
          } else {
            return item;
          }
        })
      }

    case 'ADMIN_GROUPS_DELETE':
      return {
        ...state,
        groups: state.groups.filter((item) => {
          return item.id != action.payload;
        })
      }

    default:
      return state;
  }
}

export default adminGroupReducer;