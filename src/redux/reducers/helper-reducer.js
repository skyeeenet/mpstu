const initState = {
  preloader: false,
}

const helperReducer = (state = initState, action) => {

  switch (action.type) {

    case 'SHOW_PRELOADER':
      return {
        ...state,
        preloader: true
      }

    case 'HIDE_PRELOADER':
      return {
        ...state,
        preloader: false
      }

    default:
      return state;

  }
};

export default helperReducer;