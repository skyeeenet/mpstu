const initState = {
  user: false,
  isLoaded: false,
  id: 0,
  name: '',
  image: '',
  email: '',
  faculty: '',
  password: '',
  group: '',
  theme: '',
  short_description: '',
  content: '',
  role: '',
}

const publicAccountReducer = (state = initState, action) => {

  switch (action.type) {
    case 'PUBLIC_INIT_USER':
      return {
        ...state,
        user: action.payload.status,
        role: action.payload.user.role,
        password: '',
        isLoaded: true,
        name: action.payload.user.name,
        id: action.payload.user.id,
        email: action.payload.user.email,
        group: action.payload.user.group,
        faculty: action.payload.user.faculty,
        theme: action.payload.user.theme,
        short_description: action.payload.user.short_description,
        content: action.payload.user.content,
        image: action.payload.user.profile_image,
      }

    default:
      return state;
  }

};

export default publicAccountReducer;