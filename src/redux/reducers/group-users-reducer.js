const initState = {
  users: [],
  group: '',
};

const groupUsersReducer = (state = initState, action) => {
  switch (action.type) {
    case 'PUBLIC_GROUP_USERS':
      return {
        ...state,
        users: action.payload.users,
        group: action.payload.group
      }

    default:
      return state;
  }
};

export default groupUsersReducer;