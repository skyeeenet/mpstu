const initState = {
  apiToken: '',
  success: false,
  message: '',
};

const authReducer = (state = initState, action) => {

  switch (action.type) {

    case 'AUTH_LOGIN':
      return {
        ...state,
        apiToken: action.payload.payload.apiToken,
        success: action.payload.success,
        message: action.payload.payload.message
      };

    case 'AUTH_REGISTER':
      return {
        ...state,
        apiToken: action.payload.payload.apiToken,
        success: action.payload.success,
        message: action.payload.payload.message
      }

    default:
      return state;
  }
}

export default authReducer;
