import {combineReducers} from "redux";
import authReducer from "./auth-reducer";
import userReducer from "./user-reducer";
import helperReducer from "./helper-reducer";
import userExternalReducer from "./user-external-reducer";
import publicAccountReducer from "./public-account-reducer";
import adminGroupReducer from "./admin-group-reducer";
import groupUsersReducer from "./group-users-reducer";

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  helper: helperReducer,
  userExternal: userExternalReducer,
  publicAccount: publicAccountReducer,
  adminGroup: adminGroupReducer,
  groupUsers: groupUsersReducer
});

export default rootReducer;