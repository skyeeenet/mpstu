const initState = {
  user: false,
  id: 0,
  name: '',
  image: '',
  email: '',
  faculty: '',
  password: '',
  group: '',
  theme: '',
  short_description: '',
  content: '',
  role: '',
}

const userExternalReducer = (state = initState, action) => {

  switch (action.type) {
    case 'INIT_USER':
      console.log('INIT USER');
      return {
        ...state,
        user: action.success,
        role: action.payload.role,
        password: '',
        name: action.payload.name,
        id: action.payload.id,
        email: action.payload.email,
        group: action.payload.group_id,
        faculty: action.payload.faculty,
        theme: action.payload.theme,
        short_description: action.payload.short_description,
        content: action.payload.content,
        image: action.payload.profile_image,
      }

    case 'CHANGE_FIELD':

      const fields = {
        ...state
      };
      fields[action.payload.field] = action.payload.value;
      return fields;

    case 'UPDATE_USER':
      console.log('update user');

    default:
      return state;
  }
};

export default userExternalReducer;