const initState = {
  users: [],
}

const userReducer = (state = initState, action) => {

  switch (action.type) {

    case 'USER_LIST':
      return {
        users: [
          ...action.payload
        ]
      };

    case 'DELETE_USER':
      return {
        users: state.users.filter((item) => {
          if(item.id != action.payload) return true;
          else return false;
        })
      }

    default:
      return state;
  }
}

export default userReducer;
