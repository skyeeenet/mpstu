import axios from 'axios';
import {webRoutes as config} from "../../config/web-routes";

export const loadUsers = () => {

  return (dispatch) => {

    const apiToken = localStorage.getItem('apiToken');

    dispatch({type: 'SHOW_PRELOADER'});

    axios({
      url: `${config.baseUrl}/user/get?api_token=${apiToken}`,
      method: 'get'
    }).then((resp) => {

      const users = resp.data.payload;
      console.log(users);
      dispatch({type: 'USER_LIST', payload: users});
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      localStorage.removeItem('apiToken');
      document.location = '/login';
    });
  };
};

export const deleteUser = (payload) => {

  return (dispatch) => {

    const apiToken = localStorage.getItem('apiToken');
    dispatch({type: 'SHOW_PRELOADER'});

    axios({
      url: `${config.baseUrl}/user/delete/${payload}`,
      method: 'post',
      data: {
        'api_token': apiToken
      }
    }).then((resp) => {

      dispatch({type: 'DELETE_USER', payload});
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      localStorage.removeItem('apiToken');
      document.location = '/login';
    });
  }
};

export const changeField = (payload) => {
  return (dispatch) => {
    dispatch({type: 'CHANGE_FIELD', payload});
  };
};

export const updateUser = (payload) => {
  return (dispatch) => {

    dispatch({type: 'SHOW_PRELOADER'});

    const apiToken = localStorage.getItem('apiToken');

    axios({
      url: `${config.editAccount}/${payload.id}`,
      method: 'post',
      data: {
        ...payload,
        'api_token': apiToken,
      }
    }).then((resp) => {
      dispatch({type: 'HIDE_PRELOADER'});
      console.log('Успешно обновили');
    }).catch((err) => {
      alert('Произошла ошибка');
      dispatch({type: 'HIDE_PRELOADER'});
    });

    dispatch({type: 'UPDATE_USER', payload});
  };
};

export const updateUserPhoto = (payload) => {
  return (dispatch) => {

    dispatch({type: 'SHOW_PRELOADER'});

    const apiToken = localStorage.getItem('apiToken');

    axios({
      url: `${config.editAccountPhoto}/${apiToken}`,
      method: 'post',
      headers: {
        "Content-type": "multipart/form-data",
      },
      data: payload.photo
    }).then((resp) => {
      dispatch({type: 'CHANGE_FIELD', payload: {field: 'image', value: resp.data.payload.photo}});
      dispatch({type: 'HIDE_PRELOADER'});
      console.log('Успешно обновили');
    }).catch((err) => {
      alert('Произошла ошибка');
      dispatch({type: 'HIDE_PRELOADER'});
    });
  };
};

export const groupUsers = (payload) => {
  return (dispatch) => {
    dispatch({type: 'SHOW_PRELOADER'});
    axios({
      url: `${config.groupUsers}/${payload}`,
      method: 'post',
    }).then((resp) => {
      dispatch({type: 'PUBLIC_GROUP_USERS', payload: resp.data.payload});
      dispatch({type: 'HIDE_PRELOADER'});
      console.log('Успешно обновили');
    }).catch((err) => {
      alert('Произошла ошибка');
      dispatch({type: 'HIDE_PRELOADER'});
    });
  }
}