import axios from "axios";
import {webRoutes as config} from "../../config/web-routes";

export const getGroups = () => {
  return (dispatch) => {

    dispatch({type: 'SHOW_PRELOADER'});

    const apiToken = localStorage.getItem('apiToken');

    axios({
      url: `${config.groups}`,
      method: 'post',
      data: {
        'api_token': apiToken,
      }
    }).then((resp) => {
      dispatch({type: 'ADMIN_GROUPS_INIT', payload: resp.data.payload});
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      dispatch({type: 'HIDE_PRELOADER'});
      alert('Произошла ошибка');
    });
  };
};

export const addGroup = (payload) => {

  return (dispatch) => {
    dispatch({type: 'SHOW_PRELOADER'});
    dispatch({type: 'ADMIN_GROUPS_ADD', payload});
    const apiToken = localStorage.getItem('apiToken');
    axios({
      url: `${config.addGroup}`,
      method: 'post',
      data: {
        'api_token': apiToken,
        name: payload,
      }
    }).then((resp) => {
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      dispatch({type: 'HIDE_PRELOADER'});
    });
  };
};

export const updateGroup = (payload) => {
  return (dispatch) => {
    dispatch({type: 'SHOW_PRELOADER'});
    dispatch({type: 'ADMIN_GROUPS_UPDATE', payload});
    const apiToken = localStorage.getItem('apiToken');
    axios({
      url: `${config.updateGroup}/${payload.id}`,
      method: 'post',
      data: {
        'api_token': apiToken,
        name: payload.name,
      }
    }).then((resp) => {
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      dispatch({type: 'HIDE_PRELOADER'});
    });
  };
};

export const deleteGroup = (payload) => {
  return (dispatch) => {
    dispatch({type: 'SHOW_PRELOADER'});
    dispatch({type: 'ADMIN_GROUPS_DELETE', payload});
    const apiToken = localStorage.getItem('apiToken');
    axios({
      url: `${config.deleteGroup}/${payload}`,
      method: 'post',
      data: {
        'api_token': apiToken,
      }
    }).then((resp) => {
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((err) => {
      dispatch({type: 'HIDE_PRELOADER'});
    });
  }
}
