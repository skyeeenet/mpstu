import axios from "axios";
import {webRoutes as config} from "../../config/web-routes";

export const getUserById = (payload) => {
  return (dispatch) => {

    dispatch({type: 'SHOW_PRELOADER'});

    axios({
      url: `${config.userId}`,
      method: 'post',
      data: {
        user_id: payload.user_id,
      }
    }).then((resp) => {
      dispatch({type: 'PUBLIC_INIT_USER', payload: {user: resp.data.payload, status: resp.data.success}});
      dispatch({type: 'HIDE_PRELOADER'});
    }).catch((resp) => {
      dispatch({type: 'HIDE_PRELOADER'});
    });
  };
};
