import axios from 'axios';
import {webRoutes as config} from "../../config/web-routes";
import {showPreloader} from "./helper";

export const authLogin = (payload) => {

  return (dispatch) => {

    dispatch({type: 'SHOW_PRELOADER'});

    axios({
      url: `${config.login}`,
      method: 'post',
      data: {
        email: payload.email,
        password: payload.password,
      }
    }).then((resp) => {

      dispatch({type: 'HIDE_PRELOADER'});

      console.log('AUTH LOGIN BEGIN');
      const reducerData = resp.data;

      if (reducerData.success) {
        localStorage.setItem('apiToken', reducerData.payload.apiToken);
        getUser({apiToken: reducerData.payload.apiToken})(dispatch);
      }


      dispatch({type: 'AUTH_LOGIN', payload: reducerData});
      payload.cb();
    }).catch((err) => {
      alert('Произошла ошибка');
      dispatch({type: 'HIDE_PRELOADER'});
      console.log(err);
      //localStorage.removeItem('apiToken');
    });
  };
};

export const authRegister = (payload) => {

  return (dispatch) => {
    axios({
      url: `${config.register}`,
      method: 'post',
      data: {
        name: payload.name,
        group: payload.group,
        email: payload.email,
      }
    }).then((resp) => {

      console.log('AUTH REGISTER BEGIN');
      const reducerData = resp.data;
      if (reducerData.success)
        localStorage.setItem('apiToken', reducerData.payload.apiToken);
      else
        console.log('AUTH REGISTER ERROR');
      dispatch({type: '', payload: reducerData});
    }).catch((err) => {
      console.log(err);
    });
  }
};

export const getUser = (payload) => {
  return (dispatch) => {
    axios({
      url: `${config.user}`,
      method: 'post',
      data: {
        api_token: payload.apiToken
      }
    }).then((resp) => {
      if (resp.data.success) {
        dispatch({type: 'INIT_USER', payload: resp.data.payload})
      } else {
        console.log(resp);
      }
    }).catch((err) => {
      console.log(err);
    });
  }
}

