import './App.css';
import React from 'react';
import Routes from "./Routes";
import {BrowserRouter as Router} from "react-router-dom";
import Preloader from "./components/preloader";
import {useDispatch} from "react-redux";
import {getUser} from "./redux/actions/auth";

function App() {

  const apiToken = localStorage.getItem('apiToken');
  const dispatch = useDispatch();

  if (apiToken) {
    dispatch(getUser({apiToken}));
  }

  return (
    <Router>
      <Routes />
      <Preloader />
    </Router>
  );
}

export default App;
