import {webRoutes} from "../config/web-routes";
import * as axios from "axios";

const login = (email, password) => {
  axios({
    method: 'post',
    url: webRoutes.login,
    data: {
      email,
      password
    }
  }).then((response) => {

    console.log(response);
  }).catch((err) => {

    console.log(err);
  });
};

export {login};